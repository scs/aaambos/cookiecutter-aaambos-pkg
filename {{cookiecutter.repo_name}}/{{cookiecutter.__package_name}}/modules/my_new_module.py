from __future__ import annotations

from typing import Type

from aaambos.core.supervision.run_time_manager import ControlMsg
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity


class MyNewModule(Module, ModuleInfo):
    config: MyNewModuleConfig

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        pass

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        pass

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return MyNewModuleConfig

    async def initialize(self):
        pass

    async def step(self):
        pass

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        return exit_code


@define(kw_only=True)
class MyNewModuleConfig(ModuleConfig):
    module_path: str = "{{ cookiecutter.__package_name }}.modules.my_new_module"
    module_info: Type[ModuleInfo] = MyNewModule
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0


def provide_module():
    return MyNewModule
