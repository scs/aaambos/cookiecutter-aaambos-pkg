from typing import Type

from semantic_version import Version, SimpleSpec

from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature

MY_NEW_EXTENSION_EXTENSION = "MyNewExtensionExtension"


class MyNewExtensionExtension(Extension):
    """

    """
    name = MY_NEW_EXTENSION_EXTENSION

    async def before_module_initialize(self, module):
        self.module = module
        
    async def after_module_initialize(self, module):
        pass

    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return MyNewExtensionExtensionSetup


class MyNewExtensionExtensionSetup(ExtensionSetup):
    
    def before_module_start(self, module_config) -> Extension:
        return MyNewExtensionExtension()


MyNewExtensionExtensionFeature = ExtensionFeature(
    name=MY_NEW_EXTENSION_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=MyNewExtensionExtensionSetup,
    # as_kwarg=True,
    # kwarg_alias="my_new_extension",
)



