"""
Adapt aaambos cli behavior by writing this __main__.py and use $> {{ cookiecutter.__package_name }} <command> -args ... instead.
"""
from aaambos.__main__ import main

if __name__ == '__main__':
    main()
