"""

This is the documentation of the aaambos package {{ cookiecutter.project_name }}.
It contains of {%- if cookiecutter.has_aaambos_modules -%}aaambos modules, {% endif %} 
{%- if cookiecutter.has_aaambos_com -%}an aaambos communication service, {% endif %} 
{%- if cookiecutter.has_aaambos_configs -%}owm run and/or arch configs, {% endif %} 
{%- if cookiecutter.has_aaambos_guis -%}aaambos guis, {% endif %} 
{%- if cookiecutter.has_aaambos_convs -%}aaambos architecture conventions, {% endif %} 
{%- if cookiecutter.is_aaambos_system -%}and defines an aaambos system with an own cli.{% endif %} 

# About the package

# Background / Literature

# Usage / Examples

# Citation


"""
