#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.md') as history_file:
    history = history_file.read()

requirements = ["aaambos @ git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos@main"]

test_requirements = [{%- if cookiecutter.use_pytest %}'pytest>=3',{%- endif %} ]

{%- set license_classifiers = {
    'MIT license': 'License :: OSI Approved :: MIT License',
    'BSD license': 'License :: OSI Approved :: BSD License',
    'ISC license': 'License :: OSI Approved :: ISC License (ISCL)',
    'Apache Software License 2.0': 'License :: OSI Approved :: Apache Software License',
    'GNU General Public License v3': 'License :: OSI Approved :: GNU General Public License v3 (GPLv3)'
} %}

setup(
    author="{{ cookiecutter.full_name.replace('\"', '\\\"') }}",
    author_email='{{ cookiecutter.email }}',
    python_requires='>=3.10',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
{%- if cookiecutter.license in license_classifiers %}
        '{{ license_classifiers[cookiecutter.license] }}',
{%- endif %}
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.10',
    ],
    description="{{ cookiecutter.project_short_description }}",
    {%- if cookiecutter.is_aaambos_system %}
    entry_points={
        'console_scripts': [
            '{{ cookiecutter.__package_name }}={{ cookiecutter.__package_name }}.cli:main',
        ],
    },
    {%- endif %}
    install_requires=requirements,
{%- if cookiecutter.license in license_classifiers %}
    license="{{ cookiecutter.license }}",
{%- endif %}
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords=['aaambos', '{{ cookiecutter.__package_name }}'],
    name='{{ cookiecutter.__package_name }}',
    packages=find_packages(include=['{{ cookiecutter.__package_name }}', '{{ cookiecutter.__package_name }}.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='{{ cookiecutter.pkg_home_web }}',
    version='{{ cookiecutter.version }}',
    zip_safe=False,
)