#!/usr/bin/env python
import pathlib
import shutil



cwd = pathlib.Path().resolve()
src = cwd / '{{ cookiecutter.__package_name }}'

if __name__ == '__main__':

    {% if not cookiecutter.has_aaambos_modules -%}
    shutil.rmtree(src / 'modules')
    {% endif %}
    {% if not cookiecutter.has_aaambos_extensions -%}
    shutil.rmtree(src / 'extensions')
    {% endif %}
    {% if not cookiecutter.has_aaambos_com -%}
    shutil.rmtree(src / 'communications')
    {% endif %}
    {% if not cookiecutter.has_aaambos_configs -%}
    shutil.rmtree(src / 'configs')
    {% endif %}
    {% if not cookiecutter.has_aaambos_guis -%}
    shutil.rmtree(src / 'guis')
    {% endif %}
    {% if not cookiecutter.has_aaambos_convs -%}
    shutil.rmtree(src / 'conventions')
    {% endif %}
    {% if not cookiecutter.is_aaambos_system -%}
    src.joinpath('__main__.py').unlink()
    {% endif %}

