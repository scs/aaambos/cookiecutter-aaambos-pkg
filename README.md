# Cookiecutter Template for AAAMBOS packages

Use ˋaaambos create pkgˋ which will use cookiecutter. Or just do ˋcookiecutter this_repo_clone_urlˋ

The generated package is designed to be used with
- pip
- conda
- gitlab
- ruff

